// глобальные переменные

var body_var,
		global_window_Height,
		popupOrderItem,
		controlPanelBtn,
		popupBtn,
		$completed_orders_form,
		$send_confirmation,
		$send_to_client,
		$cart_orders_form,
		$postpone_orders_form,
		$contacts_form;

function initPopup() {
    $(".vacancy_link").click(function(){
        $(".popup").show();
        $("#response_form").show();
    });
    $("#response_form .close_popup").click(function(){
        $(".popup").hide();
        $("#response_form").hide();
    });
    $("#send_resume").click(function(){
        $(".popup").show();
        $("#resume_form").show();
    });
    $("#resume_form .close_popup").click(function(){
        $(".popup").hide();
        $("#resume_form").hide();
    });
}

$(function($){
    initPopup();

	body_var = $('body'),
			global_window_Height = $(window).height(),
			popupOrderItem = $('.popup_order_item'),
			controlPanelBtn = $('.controlPanelBtn'),
			popupBtn = $('.popupBtn');

	var header = $('.header'), doc = $(document), headerPos = header.offset().top,
			browserWindow = $(window);

	var approachLeft = $('.approachLeft'), approachRight = $('.approachRight'), workApproach = $('.workApproach'), approachSplitbar = $('.approachSplitbar'), splitPressed = false, startX, startWidth, approachWidth;

	if(header.hasClass('stickable')){

		browserWindow.on('scroll', function(){

			var scrollLeft = doc.scrollLeft(), scrollTop = doc.scrollTop();

			if(/fixed/ig.test(header.css('position'))){
				header.css('marginLeft', (scrollLeft > 0 ? -scrollLeft : 0));
			}

			header.toggleClass('fixed_mod', scrollTop >= headerPos);

		});
	}

	$('.openSearchBtn').on('click', function(){  // открытие/закрытие поиска
		header.toggleClass('search_opened');
		header.removeClass('menu_opened');
		$('.searchInput').focus();
		return false;
	});

	$('.closeSearchBtn').on('click', function(){ // закрытие поиска
		header.removeClass('search_opened');
		return false;
	});

	$('.menuToggle').on('click', function(){ // открытие/закрытие меню
		header.toggleClass('menu_opened');
		header.removeClass('search_opened');

		return false;
	});

	function moveSplit(newSplitPos, animation){ // перемещение сплитбара
		workApproach.toggleClass('hide_text', newSplitPos == 0 || newSplitPos == 100);

		workApproach.addClass('no_text_selection');
		approachSplitbar.animate({'left': newSplitPos == 0 ? '1px' : newSplitPos + '%'}, animation | 0);
		approachLeft.animate({'left': newSplitPos - 100 + '%'}, animation | 0)
				.find('.approach_text').animate({'left': 100 - newSplitPos + '%'}, animation | 0);
		approachRight.animate({'right': -newSplitPos + '%'}, animation | 0)
				.find('.approach_text').animate({'left': -newSplitPos + '%'}, animation | 0, function(){
					workApproach.removeClass('no_text_selection');
					workApproach.toggleClass('show_description', newSplitPos == 0 || newSplitPos == 100);
				});

		//workApproach.removeClass('show_description');

	}

	if(approachSplitbar.length){ // инициализация сплитбара
		approachSplitbar.on('dragstart', function(e){
			return false;
		}).mousedown(function(e){ // захват сплитбара
			workApproach.addClass('no_text_selection');
			approachWidth = $('.workApproach').width();
			splitPressed = true;
			startX = e.pageX;
			startWidth = approachSplitbar.css('left').replace('px', '') * 1;
		});

		$(document).mousemove(function(e){
			if(splitPressed){
				var newSplitPos = startWidth + (e.pageX - startX);

				newSplitPos = newSplitPos > 0 ? (newSplitPos > approachWidth ? approachWidth : newSplitPos) : 0;
				newSplitPos = 100 * newSplitPos / approachWidth;

				moveSplit(newSplitPos); // перемещение сплитбара

			}
		});

		$(document).mouseup(function(){  // отпускание сплитбара
			if(splitPressed){
				splitPressed = false;
				workApproach.removeClass('no_text_selection');
			}
		});
	}

	if($('.tabsBlock').length){
		initTabs();
	}

	if($('.featureSlider').length){
		initFeatureSlider();
	}

	if($('.ctmSlider').length){
		initMainSlider();
	}

	$('.approachLeftBtn').on('click', function(){

		if(workApproach.hasClass('show_description')){
			moveSplit(100, 500);
		} else{
			moveSplit(0, 500);
		}

		return false;
	});

	$('.approachRightBtn').on('click', function(){

		if(workApproach.hasClass('show_description')){
			moveSplit(0, 500);
		} else{
			moveSplit(100, 500);
		}

		return false;
	});

	if($("#contacts_form").length){
		$contacts_form = new dialog('#contacts_form', 'dialog_global dialog_g_size_1 dialog_close_butt_mod_1', 'popupForm', false, '340', false, true);
//		$contacts_form.openDialog();
	}

	all_dialog_close();

});

function all_dialog_close(){
	body_var.on('click', '.ui-widget-overlay', all_dialog_close_gl);
}

function all_dialog_close_gl(){
	$(".ui-dialog-content").each(function(){
		var $this = $(this);
		if(!$this.parent().hasClass('always_open')){
			$this.dialog("close");
		}
	});
}

function initMainSlider(){ // инит слайдера 
	var browser_window = $(window),
			active_slide = 0,
			slide_time = 1200,
			slide_auto_scroll = 5000,
			autoHndl,
			slider_holder = $('.ctmSlider'),
			slide_width = browser_window.width(),
			slider = slider_holder.find('.slider'),
			slides = slider_holder.find('.slide'),
			slider_prev = slider_holder.find('.slider_prev'),
			slider_next = slider_holder.find('.slider_next'),
			slider_dots = slider_holder.find('.slider_bullets'),
			slider_bullet_item = slider_holder.find('.slider_bullet_item'),
			slider_bullets = slider_holder.find('.slider_bullet');

	function startAutoScroll(){
		autoHndl = setInterval(function(){
			slider_next.click();
		}, slide_auto_scroll);
	}

	function fitSliderWidth(){ // подгоняем размеры

		slide_width = browser_window.width();

		slides.css('width', slide_width);

		slider.css({
			'width'     : slide_width * (1 + slides.length),
			'marginLeft': -active_slide * slide_width
		});

		if(slider_dots.length){
			slider_dots.css({
				'width'     : slider_dots.width() * (1 + slides.length),
				'marginLeft': -active_slide * slider_dots.parent().width()
			});

			slider_bullet_item.css({'width': slider_dots.parent().width()});
		}
	}

	function setActiveSlide(){  // установка активного слайда
		slides.removeClass('active').eq(active_slide).addClass('active');
		slider_bullets.parent().removeClass('active');
		$(slider_bullets.eq(active_slide)).parent().addClass('active');

		//startAutoScroll();
	}

    startAutoScroll();
	fitSliderWidth();

	slider.append($(slides[0]).clone().addClass('ctm_clone')); // добавление клона

	if(slider_dots.length){ // добавление клона в буллеты
		slider_dots.append($(slider_bullets[0]).parent().clone().addClass('ctm_clone'));
	}

	setActiveSlide();

	slider.swipe({
		swipeLeft : function(event, direction, distance, duration, fingerCount){
			slider_next.click();
		},
		swipeRight: function(event, direction, distance, duration, fingerCount){
			slider_prev.click();
		}
	});

	browser_window.resize(function(){
		fitSliderWidth();
	});

// обраблотка стрелок

	slider_next.on('click', function(e){

		if(slider.is(':animated')) return false;

		clearInterval(autoHndl);

		active_slide++;

		if(active_slide == slides.length){

			if(slider_dots.length){
				slider_dots.animate({'marginLeft': -active_slide * slider_dots.parent().width()}, slide_time, function(){
					slider_dots.css({'marginLeft': 0});
				});
			}

			slider.animate({'marginLeft': -active_slide * slide_width}, slide_time, function(){
				slider.css({'marginLeft': 0});
				active_slide = 0;
				setActiveSlide();
			});
		} else{
			slider_dots.animate({'marginLeft': -active_slide * slider_dots.parent().width()}, slide_time);
			slider.animate({'marginLeft': -active_slide * slide_width}, slide_time, function(){
				setActiveSlide();
			});
		}

        startAutoScroll();
		return false;
	});

	slider_prev.on('click', function(e){

		if(slider.is(':animated')) return false;

		clearInterval(autoHndl);

		active_slide--;

		if(active_slide < 0){

			if(slider_dots.length){
				slider_dots.css({'marginLeft': -slider_dots.parent().width() * (slides.length)});
				slider_dots.animate({'marginLeft': -(slides.length - 1) * slider_dots.parent().width()}, slide_time);
			}

			slider.css({'marginLeft': -slide_width * (slides.length)});
			slider.animate({'marginLeft': -slide_width * (slides.length - 1)}, slide_time, function(){
				active_slide = slides.length - 1;
				setActiveSlide();
			});
		} else{
			slider_dots.animate({'marginLeft': -active_slide * slider_dots.parent().width()}, slide_time);
			slider.animate({'marginLeft': -active_slide * slide_width}, slide_time, function(){
				setActiveSlide();
			});
		}

        startAutoScroll();
		return false;
	});

	// обраблотка буллетов

	slider_bullets.on('click', function(){
		if(slider.is(':animated')) return false;

		var firedEl = $(this), newSlide = firedEl.data('slide');

		slider.animate({'marginLeft': -newSlide * slide_width}, slide_time * Math.abs(newSlide - active_slide), function(){
			active_slide = newSlide;
			setActiveSlide();
		});

		return false;
	});
}

function initTabs(){

	var tabs = $('.tabsBlock');

	tabs.each(function(){

		var tab_block = $(this);

		tab_block.find('.tab_link').on('click', function(){
			var firedEl = $(this), tabBtn = firedEl.parent();

			if(tabBtn.hasClass('active')) return false;

			tabBtn.addClass('active').siblings().removeClass('active');

			tabBtn.parents('.tabsBlock').find(firedEl.attr('href')).addClass('active').siblings().removeClass('active');

			return false;
		});

	});

}

function initFeatureSlider(){  // инит слайдера 
	var browser_window = $(window),
			active_slide = 0,
			slide_time = 500,
			slider_holder = $('.featureSlider'),
			slide_width = slider_holder.width(),
			slider = $('.Slider'),
			slides = slider.find('.featureSlide'),
			slider_prev = $('.prevFeature'),
			slider_next = $('.nextFeature'),
			slider_dots = $('.featureSliderControls'),
			slider_bullets = slider_dots.find('.featureDot');

	function fitFeatureWidth(){ // подгоняем размеры

		slide_width = slider_holder.width();

		slides.css('width', slide_width);

		slider.css({
			'width'     : slide_width * (1 + slides.length),
			'marginLeft': -active_slide * slide_width
		});
	}

	function setActiveFeature(){  // установка активного слайда
		slides.removeClass('active').eq(active_slide).addClass('active');
		slider_bullets.removeClass('active');
		$(slider_bullets.eq(active_slide)).addClass('active');
	}

	fitFeatureWidth();

	slider.append($(slides[0]).clone().addClass('ctm_clone'));  // добавление клона

	setActiveFeature();

	browser_window.resize(function(){
		fitFeatureWidth();
	});

	slider_next.on('click', function(e){

		if(slider.is(':animated')) return false;

		active_slide++;

		if(active_slide == slides.length){
			slider.animate({'marginLeft': -active_slide * slide_width}, slide_time, function(){
				slider.css({'marginLeft': 0});
				active_slide = 0;
				setActiveFeature();
			});
		} else{
			slider.animate({'marginLeft': -active_slide * slide_width}, slide_time, function(){
				setActiveFeature();
			});
		}

		return false;
	});

	// обраблотка стрелок

	slider_prev.on('click', function(e){

		if(slider.is(':animated')) return false;

		active_slide--;

		if(active_slide < 0){

			slider.css({'marginLeft': -slide_width * (slides.length)});
			slider.animate({'marginLeft': -slide_width * (slides.length - 1)}, slide_time, function(){
				active_slide = slides.length - 1;
				setActiveFeature();
			});
		} else{
			slider.animate({'marginLeft': -active_slide * slide_width}, slide_time, function(){
				setActiveFeature();
			});
		}

		return false;
	});

	slider_bullets.on('click', function(){
		if(slider.is(':animated')) return false;

		var firedEl = $(this), newSlide = firedEl.data('slide');

		slider.animate({'marginLeft': -newSlide * slide_width}, slide_time * Math.abs(newSlide - active_slide), function(){
			active_slide = newSlide;
			setActiveFeature();
		});

		return false;
	});
}